﻿using EduardoS_301048660.Models.Repositories;
using EduardoS_301048660.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EduardoS_301048660.Components
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        private IStatisticRepository _statisticRepository;
        private IClubRepository _clubRepository;

        public NavigationMenuViewComponent(IStatisticRepository statisticRepository, IClubRepository clubRepository)
        {
            _statisticRepository = statisticRepository;
            _clubRepository = clubRepository;
        }

        public IViewComponentResult Invoke()
        {
            var model = new SearchBarViewModel
            {
                Clubs = _clubRepository.Clubs.ToList(),
                Positions = _statisticRepository.Statistics.Select(p=>p.Position).Distinct().OrderBy(p=>p).ToList(),
                Years = _statisticRepository.Statistics.Select(p=>p.Year).Distinct().OrderBy(p=>p).ToList(),
                SelectedClub = (!string.IsNullOrEmpty(HttpContext?.Request?.Query["club"]) ? int.Parse(HttpContext?.Request?.Query["club"]) : 0),
                SelectedYear = (!string.IsNullOrEmpty(HttpContext?.Request?.Query["year"]) ? int.Parse(HttpContext?.Request?.Query["year"]) : 0),
                SelectedPosition = (!string.IsNullOrEmpty(HttpContext?.Request?.Query["position"]) ? HttpContext?.Request?.Query["position"].ToString() : "")
            };

            return View(model);
        }
    }
}
