﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EduardoS_301048660.Models.Poco;
using EduardoS_301048660.Models.Repositories;
using EduardoS_301048660.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EduardoS_301048660.Controllers
{
    [Authorize]
    public class ClubController : Controller
    {
        private IClubRepository _clubRepository { get; }

        public ClubController(IClubRepository clubRepository) { _clubRepository = clubRepository; }

        [AllowAnonymous]
        public IActionResult Index() => View(_clubRepository.Clubs);

        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var club = _clubRepository.Get(id.Value);

            if (club == null)
            {
                return NotFound();
            }
            return View(club);
        }

        public IActionResult Create()
        {
            if (User.Identity.Name != "Admin") return RedirectToAction("Index", "Home");
            var club = new ClubCreateEditViewModel() { ClubID = -1 };
            return View(club);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ClubCreateEditViewModel club)
        {
            if (User.Identity.Name != "Admin") return RedirectToAction("Index", "Home");
            if (ModelState.IsValid)
            {

                var obj = new Club() {
                    ClubID = club.ClubID,
                    Name = club.Name,
                    Description = club.Description,
                    Address = club.Address
                };

                _clubRepository.Add(obj);
                TempData["message"] = $"{obj.Name} has been saved";
                return RedirectToAction(nameof(Index));
            }
            return View(club);
        }

        public IActionResult Edit(int? id)
        {
            if (User.Identity.Name != "Admin") return RedirectToAction("Index", "Home");
            if (id == null)
            {
                return NotFound();
            }

            var club = _clubRepository.Get(id.Value);

            if (club == null)
            {
                return NotFound();
            }


            var obj = new ClubCreateEditViewModel()
            {
                ClubID = club.ClubID,
                Name = club.Name,
                Description = club.Description,
                Address = club.Address,
                Players = club.Players
            };

            return View(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, ClubCreateEditViewModel club)
        {
            if (User.Identity.Name != "Admin") return RedirectToAction("Index", "Home");
            if (id != club.ClubID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var obj = new Club()
                    {
                        ClubID = club.ClubID,
                        Name = club.Name,
                        Description = club.Description,
                        Address = club.Address
                    };

                    _clubRepository.Update(obj);
                    TempData["message"] = $"{obj.Name} has been saved";
                }
                catch (Exception ex)
                {
                    if (!ClubExists(club.ClubID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
           
            return View(club);
        }

        public IActionResult Delete(int? id)
        {
            if (User.Identity.Name != "Admin") return RedirectToAction("Index", "Home");
            if (id == null)
            {
                return NotFound();
            }

            var club = _clubRepository.Get(id.Value);

            if (club == null)
            {
                return NotFound();
            }
         
            return View(club);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            if (User.Identity.Name != "Admin") return RedirectToAction("Index", "Home");
            var club = _clubRepository.Get(id);

            if (club == null)
            {
                return NotFound();
            }

            _clubRepository.Delete(id);
            TempData["message"] = $"{club.Name} was deleted";
            return RedirectToAction(nameof(Index));
        }

        private bool ClubExists(int id)
        {
            return _clubRepository.Clubs.Any(e => e.ClubID == id);
        }

    }
}