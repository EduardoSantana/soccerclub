﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EduardoS_301048660.Models.Poco;
using EduardoS_301048660.Models.Repositories;
using EduardoS_301048660.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EduardoS_301048660.Controllers
{
    [Authorize]
    public class PlayerController : Controller
    {
        private IPlayerRepository _playerRepository { get; }

        private IClubRepository _clubRepository { get; }

        public PlayerController(IPlayerRepository playerRepository, IClubRepository clubRepository)
        {
            _playerRepository = playerRepository;
            _clubRepository = clubRepository;
        }

        public IActionResult Index(int id = 0)
        {
            if (id > 0)
            {
                ViewBag.ClubName = _clubRepository.Get(id).Name;
                return View(_playerRepository.Players.Where(p=>p.ClubID == id));
            }
            return View(_playerRepository.Players);
        }

        public IActionResult Details(int? id)
        {
            if (!(User.Identity.Name == "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return NotFound();
            }

            var player = _playerRepository.Get(id.Value);

            if (player == null)
            {
                return NotFound();
            }

            return View(player);
        }

        public IActionResult Create()
        {
            var model = new PlayerCreateEditViewModel() {
                ClubList = new List<SelectListItem>(),
                PlayerID = -1
            };

            foreach (var item in _clubRepository.Clubs)
                model.ClubList.Add(new SelectListItem { Value = item.ClubID.ToString(), Text = item.Name, Selected = item.ClubID == model.ClubID });

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PlayerCreateEditViewModel player)
        {
            if (ModelState.IsValid)
            {
                var obj = new Player()
                {
                    PlayerID = player.PlayerID,
                    Name = player.Name,
                    Age = player.Age,
                    Address = player.Address,
                    ClubID = player.ClubID
                };

                _playerRepository.Add(obj);
                TempData["message"] = $"{obj.Name} has been saved";
                return RedirectToAction(nameof(Index), new { id = player.ClubID });
            }

            player.ClubList = new List<SelectListItem>();

            foreach (var item in _clubRepository.Clubs)
                player.ClubList.Add(new SelectListItem { Value = item.ClubID.ToString(), Text = item.Name, Selected = (item.ClubID == player.ClubID) });

            return View(player);
        }

        public IActionResult Edit(int? id)
        {
            if (!(User.Identity.Name == "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return NotFound();
            }

            var player = _playerRepository.Get(id.Value);

            if (player == null)
            {
                return NotFound();
            }

            var obj = new PlayerCreateEditViewModel()
            {
                PlayerID = player.PlayerID,
                Name = player.Name,
                Age = player.Age,
                Address = player.Address,
                ClubID = player.ClubID,
                ClubList = new List<SelectListItem>()
            };

            foreach (var item in _clubRepository.Clubs)
                obj.ClubList.Add(new SelectListItem { Value = item.ClubID.ToString(), Text = item.Name, Selected = item.ClubID == obj.ClubID });

            return View(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, PlayerCreateEditViewModel player)
        {
            if (!(User.Identity.Name == "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id != player.PlayerID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var obj = new Player()
                    {
                        PlayerID = player.PlayerID,
                        Name = player.Name,
                        Age = player.Age,
                        Address = player.Address,
                        ClubID = player.ClubID
                    };

                    _playerRepository.Update(obj);
                    TempData["message"] = $"{obj.Name} has been saved";
                }
                catch (Exception ex)
                {
                    if (!PlayerExists(player.PlayerID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { id = player.ClubID });
            }

            player.ClubList = new List<SelectListItem>();

            foreach (var item in _clubRepository.Clubs)
                player.ClubList.Add(new SelectListItem { Value = item.ClubID.ToString(), Text = item.Name, Selected = item.ClubID == player.ClubID });

            return View(player);
        }

        public IActionResult Delete(int? id)
        {
            if (!(User.Identity.Name == "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return NotFound();
            }

            var player = _playerRepository.Get(id.Value);

            if (player == null)
            {
                return NotFound();
            }

            return View(player);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            if (!(User.Identity.Name == "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            var player = _playerRepository.Get(id);
            
            if (player == null)
            {
                return NotFound();
            }

            var clubId = player.ClubID;

            _playerRepository.Delete(id);
            TempData["message"] = $"{player.Name} was Deregistered";
            return RedirectToAction(nameof(Index), new { id = clubId });
        }

        private bool PlayerExists(int id)
        {
            return _playerRepository.Players.Any(e => e.PlayerID == id);
        }

        public IActionResult Players(int id)
        {
            return View();
        }

    }
}