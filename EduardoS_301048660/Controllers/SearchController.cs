﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EduardoS_301048660.Models.Repositories;
using EduardoS_301048660.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EduardoS_301048660.Controllers
{
    [Authorize]
    public class SearchController : Controller
    {
        private IPlayerRepository _playerRepository { get; }

        private IClubRepository _clubRepository { get; }

        public SearchController(IPlayerRepository playerRepository, IClubRepository clubRepository)
        {
            _playerRepository = playerRepository;
            _clubRepository = clubRepository;
        }

        public IActionResult Index(string query = "", string screen = "")
        {
            var model = new SearchViewModel();
            if (!string.IsNullOrEmpty(query))
            {
                model.Clubs = _clubRepository.Clubs.Where(p => p.Name.Contains(query)).ToList();
                model.Players = _playerRepository.Players.Where(p => p.Name.Contains(query)).ToList();
            }
            if (!string.IsNullOrEmpty(query))
            {
                model.Screen = screen;
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(SearchViewModel model)
        {
            if (!string.IsNullOrEmpty(model.Query))
            {
                model.Clubs = _clubRepository.Clubs.Where(p => p.Name.Contains(model.Query)).ToList();
                model.Players = _playerRepository.Players.Where(p => p.Name.Contains(model.Query)).ToList();
            }
            if (!string.IsNullOrEmpty(model.Query))
            {
                model.Screen = model.Screen;
            }
            return View(model);
        }

    }
}