﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EduardoS_301048660.Models.Repositories;
using EduardoS_301048660.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EduardoS_301048660.Controllers
{
    [Authorize]
    public class StatisticController : Controller
    {
        public int PageSize = 8;

        private IStatisticRepository _statisticRepository { get; }

        private IClubRepository _clubRepository { get; }

        public StatisticController(IStatisticRepository statisticRepository, IClubRepository clubRepository)
        {
            _statisticRepository = statisticRepository;
            _clubRepository = clubRepository;
        }

        public IActionResult Index(int club = 0, int year = 0, string position = "", int page = 1)
        {
            var model = new StatisticViewModel() {
                Club = club,
                Position = 
                position,
                Year = year
            };

            var items = _statisticRepository
                  .Statistics
                  .Where(p => model.Club == 0 || p.Club == model.Club)
                  .Where(p => model.Year == 0 || p.Year == model.Year)
                  .Where(p => string.IsNullOrEmpty(model.Position) || p.Position == model.Position);

            model.PagingInfo = new PagingInfo
            {
                CurrentPage = page,
                ItemsPerPage = PageSize,
                TotalItems = items.Count()
            };

            model.Statistics = items
                   .OrderBy(p => p.StatisticID)
                   .Skip((page - 1) * PageSize)
                   .Take(PageSize)
                   .ToList();

            if (model.Club > 0)
            {
                model.Name = _clubRepository.Get(model.Club).Name;
            }

            return View(model);
        }

        [HttpPost]
        public IActionResult Index(StatisticViewModel model)
        {
            var qry = _statisticRepository.Statistics;
            
            if (model.Club > 0)
            {
                qry = qry.Where(p => p.Club == model.Club);
                model.Name = _clubRepository.Get(model.Club).Name;
            }

            if (model.Year > 0)
            {
                qry = qry.Where(p => p.Year == model.Year);
            }

            if (!string.IsNullOrEmpty(model.Position))
            {
                qry = qry.Where(p => p.Position == model.Position);
            }

            model.Statistics = qry.ToList();
            return View(model);
        }

    }
}