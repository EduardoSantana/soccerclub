﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace EduardoS_301048660.Models
{

    public static class IdentitySeedData
    {
        private const string adminUser = "Admin";
        private const string adminPassword = "Secret123$";
        private const string generalUser = "General";
        private const string generalPassword = "Secret123$";

        public static async void EnsurePopulated(IApplicationBuilder app)
        {

            AppIdentityDbContext context = app
                .ApplicationServices
                .GetRequiredService<AppIdentityDbContext>();

            context.Database.Migrate();

            UserManager<IdentityUser> userManager = app.ApplicationServices
                .GetRequiredService<UserManager<IdentityUser>>();

            IdentityUser user = await userManager.FindByIdAsync(adminUser);
            if (user == null)
            {
                user = new IdentityUser("Admin");
                await userManager.CreateAsync(user, adminPassword);
            }

            IdentityUser userGeneral = await userManager.FindByIdAsync(generalUser);
            if (userGeneral == null)
            {
                userGeneral = new IdentityUser("General");
                await userManager.CreateAsync(userGeneral, generalPassword);
            }
        }
    }
}