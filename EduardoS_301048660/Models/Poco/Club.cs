﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EduardoS_301048660.Models.Poco
{
    public class Club
    {
        public int ClubID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public IEnumerable<Player> Players { get; set; }
    }
}
