﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EduardoS_301048660.Models.Poco
{
    public class Player
    {
        public int PlayerID { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public int Age { get; set; }

        [DisplayName("Club")]
        public int ClubID { get; set; }

        [ForeignKey("ClubID")]
        public Club Club { get; set; }

        public DateTime? ModifiedDate { get; set; }
    }
}
