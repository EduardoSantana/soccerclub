﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace EduardoS_301048660.Models.Poco
{
    public class Statistic
    {
        public int StatisticID { get; set; }
        public string Player { get; set; }
        public int Goals { get; set; }
        [DisplayName("FH")]
        public int FirstHalf { get; set; }
        [DisplayName("SH")]
        public int SecondHalf { get; set; }
        [DisplayName("FS")]
        public int FirstScorer { get; set; }
        [DisplayName("LS")]
        public int LastScorer { get; set; }
        [DisplayName("H")]
        public int Home { get; set; }
        [DisplayName("A")]
        public int Away { get; set; }
        public int Year { get; set; }
        public int Club { get; set; }
        public string Position { get; set; }
    }
}
