﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using EduardoS_301048660.Models.Poco;
using EduardoS_301048660.Infrastructure;

namespace EduardoS_301048660.Models.Repositories
{
    public class ClubRepositoryCollections : IClubRepository
    {

        public ClubRepositoryCollections(IHttpContextAccessor httpContext)
        {
            _httpContext = httpContext;
            clubs = _httpContext.HttpContext.Session.GetJsonList<Club>("Clubs") ??
                    new List<Club>()
                    {
                            new Club
                            {
                                ClubID = 1,
                                Name = "Club From Collections",
                                Address = "Scarborough",
                                Description = "New Club Descriptions",
                                ModifiedDate = DateTime.Now
                            }
                    };
            
 
        }

        private readonly IHttpContextAccessor _httpContext;

        private List<Club> clubs { get; set; }

        private void SaveClubs(List<Club> c) => _httpContext.HttpContext.Session.SetJson("Clubs", c);

        public IQueryable<Club> Clubs => clubs.AsQueryable<Club>();

        public void Add(Club club)
        {
            int id = 1;
            if (clubs.Any())
                id += clubs.Max(p=> p.ClubID);

            club.ClubID = id;
            club.ModifiedDate = DateTime.Now;
            clubs.Add(club);
            SaveClubs(clubs);
        }

        public void Update(Club club)
        {
            var ob = clubs.Where(p => p.ClubID == club.ClubID).FirstOrDefault();
            ob.Name = club.Name;
            ob.Description = club.Description;
            ob.Address = club.Address;
            ob.ModifiedDate = DateTime.Now;
            SaveClubs(clubs);
        }

        public void Delete(int id)
        {
            clubs.RemoveAll(p => p.ClubID == id);
            SaveClubs(clubs);
        }

        public Club Get(int id) => clubs.Where(p => p.ClubID == id).FirstOrDefault();
    }
}
