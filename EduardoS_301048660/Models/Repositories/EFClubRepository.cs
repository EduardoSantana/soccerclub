﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using EduardoS_301048660.Models.Poco;

namespace EduardoS_301048660.Models.Repositories
{
    public class EFClubRepository : IClubRepository
    {
        private ApplicationDbContext context;

        public EFClubRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<Club> Clubs => context.Clubs;

        public void Add(Club club)
        {
            club.ClubID = 0;
            club.ModifiedDate = DateTime.Now;
            context.Clubs.Add(club);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var c = context.Clubs.FirstOrDefault(cl => cl.ClubID == id);
            context.Clubs.Remove(c);
            context.SaveChanges();
        }

        public Club Get(int id)
        {
            return Clubs.FirstOrDefault(cl => cl.ClubID == id); 
        }

        public void Update(Club club)
        {
            var c = context.Clubs.FirstOrDefault(cl => cl.ClubID == club.ClubID);
            if (c != null)
            {
                c.Name = club.Name;
                c.Description = club.Description;
                c.Address = club.Address;
                c.ModifiedDate = DateTime.Now;
                context.SaveChanges();
            }
            
        }
    }
}
