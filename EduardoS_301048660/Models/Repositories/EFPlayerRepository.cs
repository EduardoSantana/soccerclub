﻿using EduardoS_301048660.Models.Poco;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EduardoS_301048660.Models.Repositories
{
    public class EFPlayerRepository : IPlayerRepository
    {
        private ApplicationDbContext context;

        public EFPlayerRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<Player> Players => context.Players.Include(o => o.Club);

        public void Add(Player player)
        {
            player.PlayerID = 0;
            player.ModifiedDate = DateTime.Now;
            context.Players.Add(player);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var c = context.Players.FirstOrDefault(cl => cl.PlayerID == id);
            context.Players.Remove(c);
            context.SaveChanges();
        }

        public Player Get(int id)
        {
            return context.Players.FirstOrDefault(cl => cl.PlayerID == id);
        }

        public void Update(Player player)
        {
            var c = context.Players.FirstOrDefault(cl => cl.PlayerID == player.PlayerID);
            if (c != null)
            {
                c.Name = player.Name;
                c.Age = player.Age;
                c.Address = player.Address;
                c.ModifiedDate = DateTime.Now;
                c.ClubID = player.ClubID;
            }
            context.SaveChanges();
        }
    }
}
