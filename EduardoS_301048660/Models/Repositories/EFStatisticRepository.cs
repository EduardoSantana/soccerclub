﻿using EduardoS_301048660.Models.Poco;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EduardoS_301048660.Models.Repositories
{
    public class EFStatisticRepository : IStatisticRepository
    {
        private ApplicationDbContext context;

        public EFStatisticRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<Statistic> Statistics => context.Statistics;

        public void Delete(int id)
        {
            var c = context.Statistics.FirstOrDefault(cl => cl.StatisticID == id);
            context.Statistics.Remove(c);
            context.SaveChanges();
        }

        public Statistic Get(int id)
        {
            return context.Statistics.FirstOrDefault(cl => cl.StatisticID == id);
        }
       
    }
}
