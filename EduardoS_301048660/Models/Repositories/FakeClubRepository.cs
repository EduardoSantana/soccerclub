﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EduardoS_301048660.Models.Poco;

namespace EduardoS_301048660.Models.Repositories
{
    public class FakeClubRepository : IClubRepository
    {
        public FakeClubRepository()
        {
            if (clubs == null)
            {
                clubs = new List<Club>();
            }
        }
        public static List<Club> clubs { get; set; }
        public IQueryable<Club> Clubs => clubs.AsQueryable();

        public void Add(Club club)
        {
            clubs.Add(club);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Club Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Club club)
        {
            throw new NotImplementedException();
        }
    }
}
