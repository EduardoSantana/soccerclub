﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EduardoS_301048660.Models.Poco;

namespace EduardoS_301048660.Models.Repositories
{
    public class FakePlayerRepository : IPlayerRepository
    {
        public static List<Player> players { get; set; }
        public IQueryable<Player> Players => players.AsQueryable();

        public void Add(Player club)
        {
            if (players == null)
            {
                players = new List<Player>();
            }
            players.Add(club);

        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Player Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Player club)
        {
            throw new NotImplementedException();
        }
    }
}
