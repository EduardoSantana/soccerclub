﻿using EduardoS_301048660.Models.Poco;
using System.Linq;

namespace EduardoS_301048660.Models.Repositories
{
    public interface IClubRepository
    {
        IQueryable<Club> Clubs { get; }
        void Add(Club club);
        void Update(Club club);
        void Delete(int id);
        Club Get(int id);
    }
}
