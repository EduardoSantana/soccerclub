﻿using EduardoS_301048660.Models.Poco;
using System.Linq;

namespace EduardoS_301048660.Models.Repositories
{
    public interface IPlayerRepository
    {
        IQueryable<Player> Players { get; }
        void Add(Player club);
        void Update(Player club);
        void Delete(int id);
        Player Get(int id);
    }
}
