﻿using EduardoS_301048660.Models.Poco;
using System.Linq;

namespace EduardoS_301048660.Models.Repositories
{
    public interface IStatisticRepository
    {
        IQueryable<Statistic> Statistics { get; }
        void Delete(int id);
        Statistic Get(int id);
    }
}
