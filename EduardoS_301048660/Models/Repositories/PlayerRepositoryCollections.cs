﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using EduardoS_301048660.Models.Poco;
using EduardoS_301048660.Infrastructure;

namespace EduardoS_301048660.Models.Repositories
{
    public class PlayerRepositoryCollections : IPlayerRepository
    {
        private IClubRepository _clubRepository { get; }

        public PlayerRepositoryCollections(IHttpContextAccessor httpContext, IClubRepository clubRepository)
        {
            _clubRepository = clubRepository;

            _httpContext = httpContext;
            players = _httpContext.HttpContext.Session.GetJsonList<Player>("Players") ??
                    new List<Player>()
                    {
                            new Player
                            {
                                PlayerID = 1,
                                Name = "Player From Collections",
                                Address = "Scarborough",
                                Age = 28,
                                ModifiedDate = DateTime.Now,
                                ClubID = 1,
                                Club = new Club { ClubID = 1, Name = "Name Fake" }
                            }
                    };

            foreach (var item in players)
                item.Club = _clubRepository.Get(item.ClubID);

        }

        private readonly IHttpContextAccessor _httpContext;

        private List<Player> players { get; set; }

        private void SavePlayers(List<Player> c) => _httpContext.HttpContext.Session.SetJson("Players", c);

        public IQueryable<Player> Players => players.AsQueryable<Player>();

        public void Add(Player player)
        {
            int id = 1;
            if (players.Any())
                id += players.Max(p=> p.PlayerID);

            player.PlayerID = id;
            player.ModifiedDate = DateTime.Now;
            players.Add(player);
            SavePlayers(players);
        }

        public void Update(Player player)
        {
            var ob = players.Where(p => p.PlayerID == player.PlayerID).FirstOrDefault();
            ob.Name = player.Name;
            ob.Age = player.Age;
            ob.Address = player.Address;
            ob.ModifiedDate = DateTime.Now;
            ob.ClubID = player.ClubID;
            SavePlayers(players);
        }

        public void Delete(int id)
        {
            players.RemoveAll(p => p.PlayerID == id);
            SavePlayers(players);
        }

        public Player Get(int id) => players.Where(p => p.PlayerID == id).FirstOrDefault();
    }
}
