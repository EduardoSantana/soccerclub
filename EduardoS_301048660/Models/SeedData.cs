﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System;
using System.Globalization;

namespace EduardoS_301048660.Models
{
    public static class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            ApplicationDbContext context = app
                .ApplicationServices
                .GetRequiredService<ApplicationDbContext>();

            context.Database.Migrate();
            if (!context.Clubs.Any())
            {
                context.Clubs.AddRange(
                     new Poco.Club
                     {
                         Name = "Feugiat Foundation Club",
                         Description = "Blandit Enim Consequat Institute",
                         Address = "Ecuador",
                         ModifiedDate = DateTime.ParseExact("29/05/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture)
                     },
                    new Poco.Club
                    {
                        Name = "Nunc Interdum Feugiat Inc. Club",
                        Description = "Nec Corp.",
                        Address = "Russian Federation",
                        ModifiedDate = DateTime.ParseExact("21/11/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    },
                    new Poco.Club
                    {
                        Name = "Nisi Institute Club",
                        Description = "Nisi Aenean Eget LLP",
                        Address = "Canada",
                        ModifiedDate = DateTime.ParseExact("27/02/2020", "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    },
                    new Poco.Club
                    {
                        Name = "Nullam Suscipit Ltd Club",
                        Description = "Molestie Dapibus Ligula LLC",
                        Address = "Korea, South",
                        ModifiedDate = DateTime.ParseExact("28/10/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    },
                    new Poco.Club
                    {
                        Name = "Eu LLC Club",
                        Description = "Integer Sem Institute",
                        Address = "Montserrat",
                        ModifiedDate = DateTime.ParseExact("04/04/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    }
                );
                context.SaveChanges();
            }
            if (!context.Players.Any())
            {
                context.Players.AddRange(
                    new Poco.Player { Name = "Raya V. Lucas", Age = 33, ModifiedDate = DateTime.ParseExact("24/08/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 4, Address = "Korea, South" },
                    new Poco.Player { Name = "Harding J. Gilliam", Age = 27, ModifiedDate = DateTime.ParseExact("04/01/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 1, Address = "Luxembourg" },
                    new Poco.Player { Name = "Kelsie A. Young", Age = 31, ModifiedDate = DateTime.ParseExact("18/01/2020", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 3, Address = "Niue" },
                    new Poco.Player { Name = "Jameson G. Pittman", Age = 32, ModifiedDate = DateTime.ParseExact("25/01/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 5, Address = "Bhutan" },
                    new Poco.Player { Name = "Brielle N. Rogers", Age = 27, ModifiedDate = DateTime.ParseExact("29/08/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 3, Address = "Zimbabwe" },
                    new Poco.Player { Name = "Claudia C. Knowles", Age = 29, ModifiedDate = DateTime.ParseExact("10/07/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 5, Address = "United States Minor Outlying Islands" },
                    new Poco.Player { Name = "Irma I. Barron", Age = 26, ModifiedDate = DateTime.ParseExact("10/11/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 1, Address = "Colombia" },
                    new Poco.Player { Name = "Joel K. Walker", Age = 27, ModifiedDate = DateTime.ParseExact("24/06/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 1, Address = "Niger" },
                    new Poco.Player { Name = "Zachary Z. Stephenson", Age = 27, ModifiedDate = DateTime.ParseExact("26/01/2020", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 4, Address = "Laos" },
                    new Poco.Player { Name = "Yeo D. Powers", Age = 30, ModifiedDate = DateTime.ParseExact("14/08/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 2, Address = "Cameroon" },
                    new Poco.Player { Name = "Donovan N. Fisher", Age = 25, ModifiedDate = DateTime.ParseExact("24/10/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 5, Address = "Cambodia" },
                    new Poco.Player { Name = "Glenna P. Bryan", Age = 22, ModifiedDate = DateTime.ParseExact("11/06/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 3, Address = "Thailand" },
                    new Poco.Player { Name = "Darius U. Chapman", Age = 30, ModifiedDate = DateTime.ParseExact("26/07/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 1, Address = "Kyrgyzstan" },
                    new Poco.Player { Name = "Michelle B. Pena", Age = 31, ModifiedDate = DateTime.ParseExact("19/08/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 3, Address = "Korea, North" },
                    new Poco.Player { Name = "Levi U. Meyers", Age = 22, ModifiedDate = DateTime.ParseExact("13/12/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 5, Address = "Iceland" },
                    new Poco.Player { Name = "Blaine O. Bailey", Age = 23, ModifiedDate = DateTime.ParseExact("11/03/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 5, Address = "New Zealand" },
                    new Poco.Player { Name = "Hector O. Fowler", Age = 33, ModifiedDate = DateTime.ParseExact("12/01/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 4, Address = "Guadeloupe" },
                    new Poco.Player { Name = "Amber A. Woodard", Age = 25, ModifiedDate = DateTime.ParseExact("27/08/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 2, Address = "Tuvalu" },
                    new Poco.Player { Name = "Azalia H. Bauer", Age = 32, ModifiedDate = DateTime.ParseExact("29/08/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 4, Address = "Tokelau" },
                    new Poco.Player { Name = "Theodore L. Underwood", Age = 33, ModifiedDate = DateTime.ParseExact("26/06/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 3, Address = "Maldives" },
                    new Poco.Player { Name = "Kameko I. Chambers", Age = 25, ModifiedDate = DateTime.ParseExact("24/09/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 2, Address = "Virgin Islands, British" },
                    new Poco.Player { Name = "Giacomo F. Buchanan", Age = 29, ModifiedDate = DateTime.ParseExact("20/08/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 5, Address = "Lithuania" },
                    new Poco.Player { Name = "Randall L. Powers", Age = 33, ModifiedDate = DateTime.ParseExact("18/09/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 2, Address = "Lesotho" },
                    new Poco.Player { Name = "Samantha H. Young", Age = 23, ModifiedDate = DateTime.ParseExact("01/04/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 1, Address = "Slovenia" },
                    new Poco.Player { Name = "Brynn K. Torres", Age = 22, ModifiedDate = DateTime.ParseExact("07/01/2019", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 3, Address = "Burkina Faso" },
                    new Poco.Player { Name = "Paloma L. Brady", Age = 22, ModifiedDate = DateTime.ParseExact("01/08/2018", "dd/MM/yyyy", CultureInfo.InvariantCulture), ClubID = 5, Address = "Slovakia" }
                );
                context.SaveChanges();
            }
            if (!context.Statistics.Any())
            {
                context.Statistics.AddRange(GetExampleData.GetData());
                context.SaveChanges();
            }
        }
    }
}
