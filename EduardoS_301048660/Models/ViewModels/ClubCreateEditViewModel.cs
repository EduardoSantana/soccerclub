﻿using EduardoS_301048660.Models.Poco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EduardoS_301048660.Models.ViewModels
{
    public class ClubCreateEditViewModel
    {
        public int ClubID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Address { get; set; }
        public IEnumerable<Player> Players { get; set; }
    }
}
