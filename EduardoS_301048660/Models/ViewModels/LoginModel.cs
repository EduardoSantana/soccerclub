﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EduardoS_301048660.Models.ViewModels
{

    public class LoginModel
    {

        [Required(ErrorMessage ="Error")]
        [DisplayName("Username")]
        public string Name { get; set; }

        [Required]
        [UIHint("password")]
        public string Password { get; set; }

        public string ReturnUrl { get; set; } = "/";
    }
}
