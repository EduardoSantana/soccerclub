﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EduardoS_301048660.Models.ViewModels
{
    public class PlayerCreateEditViewModel
    {
        public int PlayerID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public int Age { get; set; }
        [DisplayName("Club")]
        public int ClubID { get; set; }
        public List<SelectListItem> ClubList { get; set; }
    }
}
