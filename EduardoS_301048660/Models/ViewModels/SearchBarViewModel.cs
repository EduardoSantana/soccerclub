﻿using EduardoS_301048660.Models.Poco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EduardoS_301048660.Models.ViewModels
{
    public class SearchBarViewModel
    {
        public int SelectedClub { get; set; }
        public int SelectedYear { get; set; }
        public string SelectedPosition { get; set; }
        public List<int> Years { get; set; }
        public List<string> Positions { get; set; }
        public List<Club> Clubs { get; set; }
    }
}
