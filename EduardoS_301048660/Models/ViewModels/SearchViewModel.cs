﻿using EduardoS_301048660.Models.Poco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EduardoS_301048660.Models.ViewModels
{
    public class SearchViewModel
    {
        public string Query { get; set; }
        public string Screen { get; set; }
        public List<Club> Clubs { get; set; }
        public List<Player> Players { get; set; }
    }
}
