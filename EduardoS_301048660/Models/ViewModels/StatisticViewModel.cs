﻿using EduardoS_301048660.Models.Poco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EduardoS_301048660.Models.ViewModels
{
    public class StatisticViewModel
    {
        public string Name { get; set; }
        public int Club { get; set; }
        public int Year { get; set; }
        public string Position { get; set; }
        public string DisplayText
        {
            get
            {
                var retVal = "";

                if (!string.IsNullOrEmpty(this.Name))
                {
                    retVal += "Club " + Name;
                }

                if (this.Year > 0)
                {
                    if (!string.IsNullOrEmpty(retVal))
                    {
                        retVal += " And ";
                    }
                    retVal += " Year " + this.Year.ToString();
                }

                if (!string.IsNullOrEmpty(this.Position))
                {
                    if (!string.IsNullOrEmpty(retVal))
                    {
                        retVal += " And ";
                    }

                    retVal += "Position " + Position;
                }

                if (!string.IsNullOrEmpty(retVal))
                {
                    retVal = "for " + retVal;
                }

                return retVal;
            }
        }
        public List<Statistic> Statistics { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
